import asyncio

from core.config import BASE_URL
from services.services import (get_sub_catalog, get_pagination_pages, get_cards,
                               get_card_info_async_tasks, get_cards_info_array, create_csv)


def main():
    sub_catalog = asyncio.run(get_sub_catalog(BASE_URL))

    for task in sub_catalog:
        asyncio.run(get_pagination_pages(task[1]))

    cards = get_cards()

    for card in cards:
        asyncio.run(get_card_info_async_tasks(card))

    cards_array = get_cards_info_array()

    asyncio.run(create_csv(cards_array))


if __name__ == '__main__':
    main()

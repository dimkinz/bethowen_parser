import logging
from aiologger.loggers.json import JsonLogger

PROXY_LIST = []  # Необходимо использовать для обхода блокировок

BASE_URL = 'https://www.bethowen.ru/'

MAX_RETRIEVE = 2

DELAY = 1

HEADERS = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0'
}

logger = JsonLogger.with_default_handlers(
            level=logging.DEBUG,
            serializer_kwargs={'ensure_ascii': False},
        )

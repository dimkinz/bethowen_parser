import random

import asyncio
import aiohttp

from core.config import HEADERS, PROXY_LIST


async def async_session(url: str, headers: dict = HEADERS):
    async with aiohttp.ClientSession() as session:
        await asyncio.sleep(random.randint(1, 3))
        response = await session.get(url, headers=headers)

        # Если сработала защита от роботов, поменять прокси в цикле для сессии
        if response.status == 503:

            # Цикл бесконечный если есть блокировка!
            # Можно сделать прерывание на N по счету попытке...
            while response.status == 503:
                proxy = random.choice(PROXY_LIST)
                response = await session.get(url, headers=headers, proxy=proxy)

                if response.status == 200:
                    break

            return await response.text()

        return await response.text()


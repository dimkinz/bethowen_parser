from random import randint

import aiofiles
from aiocsv import AsyncWriter
import asyncio
from bs4 import BeautifulSoup as bs

from core.config import BASE_URL, MAX_RETRIEVE, logger
from core.session import async_session


all_cards = []
cards_info_array = []


def get_cards():
    # Получение списка собранных карточек для обхода
    return all_cards


def availability_check(func):
    # Декоратор проверки досутпности ресурса

    async def wrapper(*args):
        for retrieve in range(MAX_RETRIEVE + 1):
            response = await async_session(url=BASE_URL)
            soup = bs(response, 'html')
            title = soup.title.text.strip()

            if title in (['Сработала защита от роботов', 'Защита от роботов']) and retrieve != MAX_RETRIEVE:
                await logger.info(f'Ошибка подключения к ресурсу: {soup.title.text}')
                await asyncio.sleep(randint(5, 7))
                continue

            elif title not in (['Сработала защита от роботов', 'Защита от роботов']):
                return await func(*args)
            await logger.error(f'Ошибка подключения к ресурсу: {soup.title.text}')
            raise 'Работа парсера прервана из-за ошибки ответа сервера'

    return wrapper


@availability_check
async def get_sub_catalog(url):
    url = url + 'catalogue/'
    catalog = []
    response = await async_session(url=url)
    soup = bs(response, 'html')
    sub_catalogs = soup.find_all('div',
                          class_='item_block col-md-6 col-sm-6')

    for sc in sub_catalogs:
        sub = sc.find_all('td', class_='section_info')
        name = sub[0].find('a').text.strip()
        link = sub[0].find('a').get('href')
        sub_catalog = name, link
        catalog.append(sub_catalog)
    return catalog


@availability_check
async def get_pagination_pages(url):
    # Создаем ассинхронные таски для пагинации по подкаталогам со страницами
    url = BASE_URL[:-1] + url

    response = await async_session(url=url)
    soup = bs(response, 'html')
    pagination = soup.find('div', class_='module-pagination')
    page_count = pagination.find_all('a', class_='dark_link')
    page_count = int(page_count[-1].text)

    # Возвращаем страницы для обхода
    tasks = []

    for page in range(1, page_count + 1):
        task = asyncio.create_task(get_cards_info(url, page))
        tasks.append(task)

    await asyncio.gather(*tasks)


@availability_check
async def get_cards_info(url, page):
    # Получаем ссылки на все карточки в подкаталоге
    cards = []
    url = url + f'?PAGEN_1={page}'

    await asyncio.sleep(2)
    response = await async_session(url=url)
    soup = bs(response, 'html')

    cards_info = soup.find('div',
                           class_='category__id catalog_block items block_list dgn-flex dgn-flex-wrap'
                           ).find_all('div', class_='item_block col-4 col-md-3 col-sm-6 col-xs-6')

    for card in cards_info:
        link = card.find('a', class_='dark_link').get('href')
        cards.append(link)

    all_cards.append(cards)
    return cards


@availability_check
async def get_prop_list_urls(url, options_id):
    # Вспомогательная функция. Проход по инвариантам товара
    prod = []
    prod_list = []

    for n, opt_id in enumerate(options_id):
        current_url = BASE_URL[:-1] + url + '?oid=' + opt_id
        response = await async_session(url=current_url)
        soup = bs(response, 'html')

        scripts_soup = soup.find_all('script')
        scripts = scripts_soup[74].text.split("'NAME':'Артикул','VALUE':'")

        for script in scripts:
            if f'bx_basket_div_{opt_id}' in script:
                sku = script.split("','CODE':")[0]

        options = soup.find('div', class_='bx_size').find_all('li')
        value = options[n + 1].text
        name = soup.find('h1', class_='preview_text').text.strip()

        prod.append(sku)
        prod.append(name)
        prod.append(value)
        prod_list.append(prod)
        prod = []
    return prod_list


async def get_card_info_async_tasks(cards):
    # Создаем ассинхронные таски для сбора данных по карточкам
    tasks = []

    for card in cards:
        task = asyncio.create_task(get_card_info(card))
        tasks.append(task)

    await asyncio.gather(*tasks)

# Парсинг карточки товара
@availability_check
async def get_card_info(card_url):
    # Получаем name, sku, value из карточки
    cards_array = []
    card = []
    current_url = BASE_URL[:-1] + card_url

    await asyncio.sleep(randint(1, 3))

    response = await async_session(url=current_url)
    soup = bs(response, 'html')

    name = soup.find('h1', class_='preview_text').text.strip()
    hidden_sku = soup.find('div', style="clear:both")
    sku = hidden_sku['data-popmechanic-argument']

    options = soup.find('div', class_='bx_size').find_all('li')
    value = options[0].text

    options = options[1:]
    if len(options) > 1:
        # Если есть инварианты, запускаем обход инвариантов
        options_id = []
        for option in options:
            options_id.append(option['data-offer-id'])

        card_info = await get_prop_list_urls(current_url, options_id)

        cards_array.extend(card_info)

    card.append(sku)
    card.append(name)
    card.append(value)
    cards_array.append(card)
    cards_info_array.append(cards_array)

    return cards_array


def get_cards_info_array():
    # Возвращаем в main финальный список
    return cards_info_array


async def create_csv(array: list):
    # Сохранение результатов парсинга в файл
    async with aiofiles.open('test_async.csv', 'w') as f:
        writer = AsyncWriter(f)
        await writer.writerow(['Atricle', 'Name', 'Value'])
        await writer.writerows(array)
